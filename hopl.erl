-module(hopl).

-include_lib("wx/include/wx.hrl").

-behaviour(wx_object).

-define(CANVAS_W, 400).
-define(CANVAS_H, 400).

%% starts the app
-export([start/0]).

%% wx_object callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_event/2,
	handle_sync_event/3,
	handle_info/2, terminate/2, code_change/3]).

-record(state, {win, canvas, bitmap}).
	
start() -> wx_object:start(?MODULE, [], []).

init(Args) ->
	wx:new(Args),
	process_flag(trap_exit, true),
	Frame = wxFrame:new(wx:null(), ?wxID_ANY, "HOPL",
			[{size, {800,500}}]),
	wxFrame:createStatusBar(Frame, []),
	wxFrame:connect(Frame, close_window),

	Panel = wxPanel:new(Frame, []),
	Sizer = wxBoxSizer:new(?wxVERTICAL),

	% add button
	Button = wxButton:new(Panel, ?wxID_ANY, [{label, "Redraw"}]),
	wxPanel:connect(Button, command_button_clicked),
	wxSizer:add(Sizer, Button, [{border, 5}, {flag, ?wxALL}]),
	wxSizer:addSpacer(Sizer, 5),

	% add canvas
	Canvas = wxPanel:new(Panel, [{style, ?wxFULL_REPAINT_ON_RESIZE}]),
	wxPanel:connect(Canvas, paint, [callback]),
	wxSizer:add(Sizer, Canvas, [{flag, ?wxEXPAND},
			{proportion, 1}]),

	wxPanel:setSizer(Panel, Sizer),
	wxSizer:layout(Sizer),
	wxFrame:show(Frame),

	% set up state
	Bitmap = wxBitmap:new(new_image()),
	State = #state{win=Frame, canvas=Canvas, bitmap=Bitmap},

	{Frame, State}.

handle_call(_Msg, {_From, _Tag}, State) -> {noreply, State}.

handle_cast(_Request, State) -> {noreply, State}.

handle_sync_event(#wx{event=#wxPaint{}}, _wxObj,
		_State = #state{canvas=Canvas, bitmap=Bitmap}) ->
	DC = wxPaintDC:new(Canvas),
	draw(DC, Bitmap),
	wxPaintDC:destroy(DC),
	ok.

handle_event(#wx{id=?wxID_EXIT,
		event=#wxCommand{type=command_menu_selected}},
		State=#state{}) ->
	{stop, normal, State};

handle_event(#wx{event=#wxCommand{type=command_button_clicked}}, State = #state{}) ->
	NewBitmap = wxBitmap:new(new_image()),
	Canvas = State#state.canvas,
	DC = wxClientDC:new(Canvas),
	draw(DC, NewBitmap),
	wxClientDC:destroy(DC),
	wxBitmap:destroy(State#state.bitmap),
	{noreply, #state{win=State#state.win, canvas=Canvas, bitmap=NewBitmap}};

handle_event(#wx{event=#wxClose{}}, State = #state{win=Frame}) ->
io:format("~p Closing window ~n", [self()]),
	ok = wxFrame:setStatusText(Frame, "Closing...", []),
	{stop, normal, State};

handle_event(#wx{}, State) -> {noreply, State}.

handle_info(_Info, State) -> {noreply, State}.

%% return value is ignored for terminate
terminate(_Reason, _State = #state{win=Frame}) ->
	wxFrame:destroy(Frame),
	wx:destroy().

%% return updated internal state if successful
code_change(_OldVsn, State, _Extra) -> {ok, State}.

%% Draw the bitmap to a device context
draw(DC, Bitmap) ->
	MemoryDC = wxMemoryDC:new(Bitmap),
	wxDC:blit(DC, {0,0}, {wxBitmap:getWidth(Bitmap), wxBitmap:getHeight(Bitmap)},
			MemoryDC, {0,0}),
	wxMemoryDC:destroy(MemoryDC).

new_image() -> new_image(?CANVAS_W, ?CANVAS_H).

new_image(Width, Height) ->
	wxImage:new(Width, Height, list_to_binary(image_data(Width, Height))).

%% TODO draw a circle (assignment)
%% returns list of image bytes [R,G,B,R,G,B,...]
image_data(Width, Height) ->
	rgb_repeat([<<127>>, <<0>>, <<0>>], Width*Height).

%% just repeat an RGB triple a number of times
rgb_repeat(RGB, Num) ->
	case Num of
		0 -> [];
		_ -> RGB ++ rgb_repeat(RGB, Num-1)
	end.

